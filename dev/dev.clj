(ns dev
  (:require [io.pedestal.service.http :as bootstrap]
            [graph-experimental.service :as service]
            [graph-experimental.server :as server]
            [plumbing.core :refer [fnk]]
            [plumbing.graph :as graph]))

;; To update routes, reload service.clj and re-evaluate dev-routes.
;; I'm sure this can be improved.
(def dev-routes
  (:routes ((graph/eager-compile service/routes-graph) {})))

(def dev-environment
  {
   :env     (fnk [] :dev)
   :service (fnk [core-service routes]
                 (->
                      (merge core-service
                             {;; do not block thread that starts web server
                              ::bootstrap/join? false
                              ;; reload routes on every request
                              ::bootstrap/routes #(deref #'dev-routes)
                              ;; all origins are allowed in dev mode
                              ::bootstrap/allowed-origins (constantly true)})
                      (bootstrap/default-interceptors)
                      (bootstrap/dev-interceptors)))})

(def dev-service-instance
  ((graph/lazy-compile
    (merge server/default-server service/service-graph dev-environment)) {}))

(defn start
  []
  ((:start dev-service-instance)))

(defn stop
  []
  ((:stop dev-service-instance)))

(defn restart
  []
  (stop)
  (start))

(comment
  (def service (-> service/service ;; start with production configuration
                   (merge  {:env :dev
                            ;; do not block thread that starts web server
                            ::bootstrap/join? false
                            ;; reload routes on every request
                            ::bootstrap/routes #(deref #'service/routes)
                            ;; all origins are allowed in dev mode
                            ::bootstrap/allowed-origins (constantly true)})
                   (bootstrap/default-interceptors)
                   (bootstrap/dev-interceptors))))
