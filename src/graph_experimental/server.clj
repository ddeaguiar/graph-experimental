(ns graph-experimental.server
  (:gen-class) ; for -main method in uberjar
  (:require [graph-experimental.service :as service]
            [io.pedestal.service.http :as bootstrap]
            [plumbing.core :refer [fnk]]
            [plumbing.graph :as graph]))

(def default-server {
                     :env (fnk [] :prod)
                     :type (fnk [] :jetty)
                     :port (fnk [] 8080)
                     :resource-path (fnk [] "/public")
                     :core-service (fnk [routes env type port resource-path]
                                   {:env env
                                    :type type
                                    :port port
                                    ::bootstrap/routes routes
                                    ::bootstrap/port port
                                    ::bootstrap/type type
                                    ::bootstrap/join? false
                                    ::bootstrap/resource-path resource-path})
                     :service (fnk [core-service] core-service)
                     :server  (fnk [service] (bootstrap/create-server service))
                     :start   (fnk [server] #(bootstrap/start server))
                     :stop    (fnk [server] #(bootstrap/stop server))
                     })

(def service-instance ((graph/eager-compile (merge service/service-graph default-server)) {}))

(defn -main [& args]
  ((:start service-instance))
  ((:stop service-instance)))






;; Container prod mode for use with the io.pedestal.servlet.ClojureVarServlet class.

(comment
  (defn servlet-init [this config]
    (alter-var-root #'service-instance
                    (constantly (bootstrap/create-servlet service/service)))
    (.init (::bootstrap/servlet service-instance) config))

  (defn servlet-destroy [this]
    (alter-var-root #'service-instance nil))

  (defn servlet-service [this servlet-req servlet-resp]
    (.service ^javax.servlet.Servlet (::bootstrap/servlet service-instance)
              servlet-req servlet-resp)))
