(ns graph-experimental.service
    (:require [io.pedestal.service.http :as bootstrap]
              [io.pedestal.service.http.route :as route]
              [io.pedestal.service.http.body-params :as body-params]
              [io.pedestal.service.http.route.definition :refer [defroutes expand-routes]]
              [io.pedestal.service.interceptor :as h]
              [ring.util.response :as ring-resp]
              [plumbing.core :refer [fnk]]
              [plumbing.graph :as graph]))

(def routes-graph
  (graph/graph
   {
    :home-page-fn (fnk [] (fn [request]
                            (ring-resp/response "Hello World!")))
    :about-page-fn (fnk [] (fn [request]
                             (ring-resp/response (format "Clojure  %s"
                                                         (clojure-version)))))
    :routes (fnk [home-page-fn about-page-fn]
                 (expand-routes
                  [[["/" {:get [(h/handler ::home-page home-page-fn)]}
                     ;; Set default interceptors for /about and any other paths under /
                     ^:interceptors [(body-params/body-params) bootstrap/html-body]
                     ["/about" {:get [(h/handler ::about about-page-fn)]}]]]]))}))

(def service-graph
  (graph/graph
   {
    ;; I'm sure there's a better way to associate the graphs.
    :routes  (fnk [] (:routes ((graph/eager-compile routes-graph) {})))
    :service (fnk [routes]
                  {:env :prod
                   ::bootstrap/routes routes
                   ::bootstrap/resource-path "/public"
                   ::bootstrap/type :jetty
                   ::bootstrap/port 8080})}))
